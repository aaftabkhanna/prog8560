**Instructions :**

Step 1 : Clone in your directory
Step 2 : Type npm install as you are in the folder 
Step 3 : Type node PROJECT NAME to run the file 

**MIT LICENSE** 
Using the MIT License is very helpul. It is welcoming to open source developers. This quality of the license allows it to be both business-friendly and open-source friendly, while still making it possible to be monetized and that is why I chose this as LICENSE.
