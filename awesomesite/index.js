// import dependicies

var express = require ('express');
var path = require ('path');

var myApp = express ();

//set path to the public folders and views folders.

myApp.set ('views', path.join (__dirname, 'views'));
myApp.use (express.static(__dirname + '/public'));

myApp.set ('view engine', 'ejs');

//Set up different routes (pages)

//home page 
myApp.get ('/', function (req, res)
{
    res.render('form'); // no need to add .ejs extension to the command.
});

//Author
myApp.get('/author', function (req,res){
    res.render('author',{
        name : "admin",
        studentNumber : '323456'
    });
});

//start server and listen to port
myApp.listen(8080); // Open URL in Browser: http://localhost:8080

//Confirmation Output
console.log('Excution Complete... Website opened at port 8080!');